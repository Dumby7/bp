\chapter{Roztroušená skleróza mozkomíšní}
Cílem této kapitoly je seznámit čtenáře s~nemocí, pro jejíž rehabilitaci byl software primárně vytvářen.\par

Roztroušená skleróza mozkomíšní (Sclerosis multiplex cerebrospinalis) je chronické onemocnění centrální nervové soustavy (mozku a míchy) \cite{Ambler}.  V~České republice se jedná o~poměrně časté onemocnění, trpí jím přibližně 10000 až 13000 lidí \cite{Rasova}, tzn. zhruba každý 1000, přičemž výskyt nemoci u~žen je asi 2x-3x častější než u~mužů \cite{RSnelekar}. Dodnes nelze jednoznačně říci co je příčinou vzniku nemoci. To má za důsledek, že jí nelze předejít. Její vývoj je dnes přičítán jak faktorům genetickým, tak faktorům zevního prostředí \cite{Ambler}. Na základě praxe a experimentů jsou známy mechanismy a spouštěče, které vedou k~rozvinutí nemoci. Nemoc se typicky začne projevovat mezi 20.-30. rokem života. Dosud nebyl nalezen lék, který by průběh nemoci zastavil úplně. Sama o~sobě není smrtelná, ovšem jelikož se jedná o~onemocnění celoživotní, může pacienta v~určitých oblastech značně omezovat \cite{CojeRS}.


\section{Průběh nemoci}
Roztroušená skleróza, dále jen \acrshort{RS}, se řadí mezi autoimunitní, demyielinizační choroby \cite{Ambler}. Základní funkcí imunitního systému člověka je chránit tělo před infekcemi a napadání cizích, nebezpečných buněk. U~autoimunitních onemocnění ovšem dochází k~tomu, že buňky imunitního systému začnou napadat a ničit buňky tkání organismu. Takovýchto nemocí je celá řada, typicky se dělí podle toho jaký typ tkáně napadají \cite{RSnelekar}. Při \acrshort{RS} dochází k~tomu, že T-lymfocyty, makrofágy a další bílé krvinky imunitního systému, začnou přecházet přes hematoencefalickou bariéru (přechod mezi vlásečnicemi krevní soustavy a mozkové tkáně). Následně začnou považovat myelin za cizí a postupně napadají a ničí myelinové pochvy v~bílé hmotě mozku a míchy, odtud název demyielinizační. Přičemž nelze jednoznačně určit, co tuto reakci imunitního systému způsobuje \cite{Ambler}.\par

Myelinová pochva vytváří lipidový (tukový) obal okolo axonů, dlouhých výběžků neuronů (buňky nervové soustavy), které zajišťují přenos akčních potenciálů mezi jednotlivými buňkami. Pochva je přerušována Ranvierovými zářezy, viz obrázek \ref{fig:neuron}. Myelin je v~centrální nervové soustavě tvořen oligodendrocyty a jeho účelem je chránit a vyživovat samotná nervová vlákna. Myelin také umožňuje přeskakování impulsů z~jednoho Ranvierova zářezu na druhý, místo toho aby se vzruch musel šířit po celém vlákně, čímž šíření jednotlivých signálů značně urychluje \cite{CojeRS}.\par

\begin{figure}
    \centering
    \includegraphics[width=10cm,height=10cm, keepaspectratio]{images/neuron.jpg}
    \caption[Neuron - základní jednotka nervové soustavy]{Neuron - základní jednotka nervové soustavy\footnotemark}
    \label{fig:neuron}
\end{figure}
\footnotetext{\url{https://velkaencyklopedie.estranky.cz/fotoalbum/biologie/biologie-lidske-telo/nervova-soustava/neuron.-.html}}

Při rozpadu myelinových pochev dochází ke chronickému zánětu, a vzniká až několik nepravidelně rozmístěných ložisek, kterým se také říká plaky nebo jinak léze. Odtud také pochází název \emph{roztroušená skleróza}, jelikož po odeznění zánětu vznikají v~místech postižení jizvy, řecké skleros znamená tuhý. Roztroušená protože ložiska mohou vznikat na různých místech mozku a míchy. Velikost ložisek může být různá, od milimetrů po centimetry \cite{CojeRS}. V~určitých případech jsou nervová vlákna schopna alespoň částečné reparace, tzv. remyelinizace \cite{Ambler}. Ovšem postupem času dochází k~vyčerpání regeneračních schopností oligodendrocytů. Důsledkem toho sice dojde k~obnovení vzruchů, jejich vedení je ale pomalejší \cite{CojeRS}. V~pozdějších, někdy ale i v~časných, fázích nemoci může také docházet k~úplnému zničení samotných axonů, v~takových to případech již není možná obnova funkce dané buňky \cite{Ambler}.\par

Vlastní průběh nemoci je poměrně různorodý a nevypočitatelný, může se u~každého pacienta lišit \cite{CojeRS}. Typickým průběhem nemoci je střídání \emph{atak} (relapsů) a \emph{remisí}. Ataka je období nemoci, kdy dochází k~rozvoji neurologické dysfunkce, tedy období, kdy se buď zhoršují stávající příznaky, nebo dochází k~rozvinutí nových. Po atakách následuje období, kdy dochází k~úplnému, nebo alespoň částečnému, uzdravení (remisi). Pacient se cítí dobře a neurologický nález je také v~normě \cite{Ambler}. U~některých pacientů dochází k~tomu, že první záchvat zanechá trvalé následky, ale nemoc se dále nezhoršuje, nebo může od prvního záchvatu docházet k~neustálému zhoršování. Po první atace typicky dochází k~dalším relapsům po dvou letech, ovšem může nastat i dříve, ale jsou i případy, kdy se další ataka neprojeví nikdy \cite{CojeRS}. První atace nejčastěji předchází nějaký negativní vnější faktor, například virové onemocnění, psychický a fyzický stres, ale může se projevit i zcela náhle  \cite{Ambler}. Podle průběhu se RS dělí na \emph{benigní} a \emph{maligní}. Benigní forma se projevuje jen malým množstvím atak s~nepatrnými následky, maligní typ je charakterizován těžkými záchvaty s~rychlým rozvojem invalidity \cite{CojeRS}.


\subsection{Typy roztroušené sklerózy}\label{rs-types}
Na základě průběhu nemoci, popsanému výše, se pak nemoc dělí do čtyř kategorií:

\begin{itemize}
    \item \textbf{Relaps-remitentní} --- jedná se o~nejběžnější typ \acrshort{RS}, vyskytuje se u~většiny (\textasciitilde85\%) pacientů v~počátcích onemocnění \cite{BPKvZivRS}. Mohou se vyskytnout spontánní remise,  kdy je stav nemocného stabilní, bez jakékoliv léčby. To lze s~největší pravděpodobností připsat procesu \emph{remyelinizace} \cite{CojeRS}. Období trvá přibližně od 5 do 15let. Probíhá zde zánětlivý proces, který je stále ovlivnitelný léky. Zhruba polovina nemocných přejde do deseti let z~formy relaps–remitentní do chronicko–progresivní \cite{BPKvZivRS}.
    
    \item \textbf{Chronicko-progresivní} --- neboli sekundárně progresivní. V~tomto období dochází ke snížení zánětlivé činnosti a naopak růstu neurodegenerace.Typicky organismus v~této fázi již vyčerpal schopnost remyelinizace a proto zde další poškození způsobené zánětem nese trvalé následky. Ataky proto nebývají tak nápadné a spíše dochází k~postupnému nárůstu invalidity až imobilizaci nemocného. Léčba léky už zde přestává být efektivní a kvalita života pacienta záleží především na jeho životosprávě a rehabilitacích \cite{BPKvZivRS}.
    
    \item \textbf{Primárně-progresivní} --- touto formou onemocnění trpí přibližně 10\% pacientů, převážně se jedná o~muže pozdního věku (40--50 let). U~tohoto typu jsou remise velmi málo časté, a tak dochází již od počátku onemocnění k~pozvolnému nárůstu invalidity \cite{CojeRS}. Svým průběhem se zcela liší od relaps-remitentní formy, převažuje neurodegenerace a úbytek oligodendrocytů nad zánětlivou činností \cite{BPKvZivRS}.
    
    \item \textbf{Relabující-progresivní} --- nejméně často vyskytující se forma. Na druhou stranu také nejhorší ze všech popsaných. Charakterizuje se tím, že po atakách nedochází u~pacientů k~žádnému zlepšení stavu \cite{CojeRS}. Nachází se zde zvýšená jak zánětlivá tak neurodegenerační činnost, které vedou k~invaliditě již během pár let od počátku onemocnění \cite{BPKvZivRS}.
\end{itemize}



\section{Symptomy}
Příznaky s~sebou přináší řadu poruch, které korespondují s~konkrétním místem demyelinizace v~centrální nervové soustavě. Kromě místa postižení je projev choroby taky částečně ovlivněn velikostí zánětu. Typicky jsou první symptomy onemocnění mírného charakteru, které jsou dosti nespecifické, např. únava, deprese nebo bolesti končetin, pacienti jim nevěnují větší pozornost, a proto je sdělují lékařům až po delší době \cite{BPKvZivRS}.\par

\begin{itemize}
    \item \textbf{Senzitivní poruchy} --- jsou poruchy citlivosti ve smyslu parestézie a dysestésie v~různých částí těla především v~horních a dolních končetinách. Jedná se o~nepříjemný pocit brnění, píchání, svědění či pálení kůže. Tyto symptomy se vyskytují zhruba u~poloviny nemocných \cite{Ambler} a často jsou přehlíženy jelikož po pár dnech až týdnech odezní \cite{BPKvZivRS}.
    
    \item \textbf{Poruchy motoriky}\label{motor-disorders} --- jedná se o~\emph{nejvýznamnější} projevy \acrshort{RS}, způsobené poruchou pyramidové dráhy\footnote{Nervová dráha vedoucí z~motorické kůry mozku a končící v~míšních segmentech.} jejíž funkcí je volní\footnote{Volní motorika je schopnost organismu provádět vůlí cílené pohyby.} a především jemná motorika končetin \cite{BPKvZivRS}. Poruchy tohoto typu se typicky neprojevují na začátku onemocnění, ale až v~pozdější fázi \cite{Ambler}. Jedná se především o~parézy (obrny) a spasticitu (porucha způsobená zvýšením tonických napínacích reflexů, závislých na rychlosti protažení svalu, tedy čím rychleji je prováděn napínací pohyb, tím větší je kladený odpor příslušných svalových segmentů \cite{Rasova}). Poruchy mohou postihnout kteroukoliv z~končetin (i více najednou), nejčastěji se postižení objevuje u~dolních končetin \cite{BPKvZivRS}. Časté jsou problémy při chůzi, kdy si pacient není jistý vlastní rovnováhou \cite{Rasova}. Pacienti mohou pociťovat svalovou slabost a zvýšení svalového napětí, které vedou až ke křečím, problémy s~ohybem končetin nebo svalový třes. Projevy se mohou v~pozdní fázi různě kombinovat, což je nejčastější příčinou vedoucí k~invaliditě daného nemocného. Při nečinnosti svalů se projevy pouze zhoršují, dají se ovšem ovlivnit léčbou, především rehabilitacemi \cite{BPKvZivRS}.
    
    \item \textbf{Únava} --- jeden z~nejběžněji vyskytujících se příznaků neurologických onemocnění, který výrazně omezuje společenský život a vykonávání běžných denních aktivit \cite{Rasova}. Jedná se o~velmi obtěžující nespecifický symptom, kterým trpí asi 95\% osob s~\acrshort{RS}. U~každého se může projevovat trochu jinak. Je pociťována jako nedostatek síly, pocit vyčerpání bez odpovídající fyzické zátěže a ztráta energie \cite{BPKvZivRS}. Na vzniku únavy se podílí například svalová slabost, bolest, různá onemocnění, psychický stav nebo poruchy spánku \cite{Rasova}.
    
    \item \textbf{Psychické potíže} --- jedná se o~změny afektivity a to zejména o~deprese nebo někdy o~euforie. Intelekt pacienta nebývá nemocí ovlivněn \cite{Ambler}. Depresivní syndromy se vyskytují zhruba u~poloviny pacientů a je prokázáno, že u~takovýchto pacientů je riziko sebevraždy asi 7x větší než u~lidí zdravých, symptomy by se tedy neměly přehlížet. Symptomy se projevují jako nekontrolovatelný pláč, nebo v~případě euforie smích. Pacienti pociťují strach, úzkost a sebelítost, což vede ke zhoršení kvality života, jelikož mají větší tendence k~sociální izolaci, narušování rodinných vztahů. U~některých pacientů může také docházet ke zhoršení kognitivních (paměť, myšlení, koncentrace\ldots) funkcí \cite{BPKvZivRS}.
    
    \item \textbf{Další} --- mezi další typické příznaky patří například močové potíže, mozečkové poruchy (špatná koordinace, skandovaná řeč\ldots). Pro více informací o~těchto a dalších projevech viz \cite{Ambler}, \cite{BPKvZivRS}.
\end{itemize}



\section{Léčba}
Jak již bylo zmíněno roztroušenou sklerózu \emph{není} možné zcela vyléčit.\\ U~řady pacientů lze ovlivnit průběh nemoci. Léčebný proces se pak rozlišuje na léčbu akutního stavu, kdy dochází u~pacienta k~relapsům (zhoršení příznaků), a na dlouhodobou léčbu, kdy je snaha snížit počet atak a zpomalit rozvoj nemoci \cite{BPKvZivRS}.\par

Dále zde léčebné zásahy pro léčbu symptomů a atak popisovat neubudu. Jedná se převážně o~podávání léků pacientům, ať už za účelem zlepšení jejich aktuálního stavu, či prevenci jeho zhoršení. Pro více informací na toto téma odkážu na \cite{Ambler} a \cite{BPKvZivRS}. Popíši zde proces rehabilitací, který má v~rámci kontextu práce větší význam.


\subsection{Rehabilitace}
\emph{\uv{Rehabilitace využívá multidisciplinárních strategií ke zvýšení funkční nezávislosti, prevenci komplikací a zlepšení kvality života nemocných. Jde o~aktivní proces, který pomáhá lidem k~zotavení, k~zachování optimální fyzické, smyslové, intelektové, psychické a sociální úrovně funkcí a k~dosažení co nejvyšší úrovně nezávislosti navzdory omezení, které onemocnění způsobuje \cite{Rasova}.}}\par

Rehabilitační postup vychází ze stanovené diagnózy pacienta. Nejprve se určí základní onemocnění, po němž následuje analýza problému rehabilitačním týmem (lékař, fyzioterapeut, psycholog\ldots) pro stanovení priorit léčby. Léčba je postupně modifikována a hodnocena \cite{Rasova}. Vzhledem k~variabilitě onemocnění je pro každého pacienta vytvořen individuální rehabilitační program, při kterém se mimo jiné bere v~potaz i fáze, ve které se nemocný nachází \cite{BPKvZivRS}.\par

Častým problémem bývají depresivní syndromy. Jelikož člověk trpící depresemi není schopen spolupracovat na rehabilitačním procesu, je třeba je včas identifikovat a léčit. Samotné rehabilitace mají poměrně velký vliv na ovlivnění psychiky, a to především rehabilitace individuální, jelikož je snaha vytvářet klidné a bezpečné prostředí. Terapeuti s~nemocnými vedou rozhovor, věnují jim čas a snaží se je nutit na sobě pracovat a zlepšovat se, což vede k~lepšímu psychickému stavu \cite{Rasova}.\par

Rehabilitace jsou zaměřeny především na spasticitu, zvýšení svalové síly a poruchu koordinace\footnote{Jedná se tedy o~poruchu především motorické.} \cite{BPKvZivRS}. U~těchto poruch nemocný váhá, jak má vlastně daný pohyb udělat a dochází tedy ke zpomalení již naučených činností a zhoršení obratnosti. Cílem rehabilitace je tedy motorické řízení a učení. Nejprve dojde k~plánování pohybové sekvence a po prvním úspěšném výkonu je snaha o naučení dané dovednosti opakujícím se tréninkem, kterým dochází ke~zdokonalování provedení pohybu (přesnost, rychlost, plynulost) \cite{Rasova}.\par

Pacient s~terapeutem provádí několik cvičení (pohybů). Nejprve je pacientovi vysvětlen princip řízení daného pohybu a princip jeho terapie. Následuje definice pohybu a nastavení pacienta do výchozí polohy pro vykonání. Samotný pohyb pak bývá doprovázen s~pomocí terapeuta, který pacienta navádí a pomáhá k~tomu, aby byl pohyb vykonáván korektně. Je důležité, aby pohyby byly vykonávány správně, jelikož při špatném provádění cvičení může dojít spíše k~prohloubení symptomů \cite{Rasova}.\par

Mezi jednotlivá cvičení patří například správné sezení, zvedání se ze sedu do stoje, chůze, diagonály horních končetin, kreslení spirál nebo špetkový úchop.